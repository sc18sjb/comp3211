.PHONY: all 
.PHONY: build-separator build-convertor build-client
.PHONY: run-separator run-convertor run-client
.PHONY: separator convertor client

all: build-separator build-convertor separator convertor client

ifeq ($(origin CLIENT_PORT),undefined)
    export CLIENT_PORT=80
endif

build-separator:
	docker build -t separator separator
build-convertor:
	docker build -t convertor convertor
build-client:
	docker build -t client client

run-separator:
	docker run -d -p 8081:8080 separator
run-convertor:
	docker run -d -p 8082:8080 convertor
run-client:
	docker run -d -p $(CLIENT_PORT):8080 client

separator: build-separator run-separator
convertor: build-convertor run-convertor
client: build-client run-client