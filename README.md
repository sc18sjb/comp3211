# COMP3211 - Distributed Systems

## Requirements 

- Docker

## Setup

To run the integrated client simply call `make client`. 

You can now navigate to 'localhost' in your browser.

The default port it 80, to manually change that you can set the CLIENT_PORT environment variable e.g. `CLIENT_PORT=8080 make client`.  

Otherwise, call docker directly: ` docker build -t client client` and `docker run -it -p 80:8080 client`.

## Hosting

We have hosted our other services on the internet.  The client will call these deployments.  Having said that, all other services
can be built and ran with docker too from the commandline: `make all`

To see the client live deployment, go: http://212.71.245.186/

## Demo
We have a youtube video showing how the system works here: https://youtu.be/-6DdU4Ya388

## Deployment 

If for whatever reason you cannot get the integrated cient working locally, we have deployed it online to let you use it.

## Repo

To view the integrated clients code navigate to **/client**

To view Web Service's 1 code - Audio Separator, navigate to **/separator**

To view Web Service#s 2 code - Audio Converter, navigate to **/converter**

We also wrote a report using LaTeX before realising the GradeScope submission, this can be viewed under **/report**
