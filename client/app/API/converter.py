import os
import requests
import zipfile
from app import app
from requests.auth import HTTPBasicAuth
from flask import Blueprint, request, jsonify

converterApi = Blueprint('converterApi', __name__)

def convertWav(youtubeId, bitrate, filetype, filename):
    audio = open(
        os.path.join(app.root_path, f'static/uploads/separator/{youtubeId}/{filename}'),
        'rb')

    url = 'http://176.58.123.162:80/converter'
    if filetype.strip() and filetype is not None:
        url += ('/' + str(filetype))
    if bitrate.strip() and bitrate is not None:
        url += ('/' + str(bitrate))

    response = requests.post(
        url,
        files={'audio': audio},
        auth=HTTPBasicAuth('admin', 'test'))

    if response.status_code == 200:
        audio = response.content
        filepath = os.path.join(app.root_path, f'static/uploads/converter/{filename}-{youtubeId}.{str(filetype)}')
        with open(filepath, 'wb') as f:
            f.write(audio)
            f.close()
        
        return {'status': 'ok', 'file': filepath}
    return {'status': 'fail', 'msg': response}


@converterApi.route('/convert/wav', methods=["POST"])
def converter():
    vid = request.form.get('vid')
    bitrate = request.form.get('bitrate')
    filetype = request.form.get('filetype')
    app.logger.info(f"Starting convert from wav for youtube video ID: {vid} to bitrate: {bitrate} and filetype: {filetype}")

    try:
        filepath = os.path.join(app.root_path, f'static/uploads/separator/{vid}')
        files = []
        for f in os.listdir(filepath):
            if not os.path.isfile(os.path.join(filepath, f)):
                continue
            result = convertWav(vid, bitrate, filetype, f)

            if result['status'] != 'ok':
                app.logger.error(f"Error in convert from wav for youtube video ID: {vid} with message: {str(result['msg'].reason)}")
                return jsonify({'error': result['msg'].reason})
            
            files.append(result['file'])
        
        filename = f'static/uploads/archives/separator-{vid}.zip'
        with zipfile.ZipFile(os.path.join(app.root_path, filename), 'w') as archive:
            for f in files:
                archive.write(f, os.path.basename(f))
            archive.close()

        return jsonify({'success': filename})
    
    except Exception as e:
        app.logger.error(f"Error in convert from wav for youtube video ID: {vid} with message: {str(e)}")
        return jsonify({'error': str(e)})