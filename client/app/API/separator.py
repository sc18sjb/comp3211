import os
import io
import zipfile
import requests
import shutil
from app import app
from flask import Blueprint, request, jsonify

separatorApi = Blueprint('separatorApi', __name__)

def separateMp3(youtubeId, stems):
    audio = open(
        os.path.join(app.root_path, f"static/uploads/youtube/{youtubeId}.mp3"),
        'rb')
    response = requests.post(
        f'http://176.58.112.24/separate/{int(stems)}',
        files={'file': audio})

    if response.status_code == 200:
        audioZip = response.content
        z = zipfile.ZipFile(io.BytesIO(audioZip))
        folderpath = os.path.join(app.root_path, f'static/uploads/separator/{youtubeId}')

        if os.path.isdir(folderpath):
            shutil.rmtree(folderpath)
        os.mkdir(folderpath)
    
        z.extractall(os.path.join(app.root_path, f'static/uploads/separator/{youtubeId}'))
        return {'status': 'ok'}
    return {'status': 'fail', 'msg': response}


@separatorApi.route('/separate/mp3', methods=["POST"])
def separator():
    vid = request.form.get('vid')
    stems = request.form.get('stems')
    app.logger.info(f"Starting separate mp3 for youtube video ID: {vid}")

    try:
        result = separateMp3(vid, stems)
        if result['status'] != 'ok':
            app.logger.error(f"Error in separate mp3 for youtube video ID: {vid} with message {str(result['msg'].reason)}")
            return jsonify({'error': result['msg'].reason})
        else:
            app.logger.info(f"Finished separate mp3 for youtube video ID: {vid}")
            return jsonify({'success': True})

    except Exception as e:
        app.logger.error(f"Error in separate mp3 for youtube video ID: {vid} with message: {str(e)}")
        return jsonify({'error': str(e)})
