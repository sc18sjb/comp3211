import os
import secrets
import requests
import urllib.request
from app import app
from flask import Blueprint, request, jsonify
import time

youtubeApi = Blueprint('youtubeApi', __name__)


def convertToMp3(youtubeId):
    headers = {
        'x-rapidapi-host': "youtube-mp36.p.rapidapi.com",
        'x-rapidapi-key': app.config['X_RAPIDAPI_KEY']
    }

    response = requests.get("https://youtube-mp36.p.rapidapi.com/dl", headers=headers, params={'id': youtubeId})
    app.logger.info(f"Convert to mp3 returned: {response.status_code}")

    # Begin conversion
    if response.status_code == 200:
        status = response.json()['status']
        app.logger.info(f"Convert to mp3 STATUS: {status}")

        # We're still processing the download, so we'll poll the API again after 2 seconds
        if status == "processing":
            time.sleep(2.0)
            response = convertToMp3(youtubeId)
            return response

        # We doenload the mp3 locally to the machine for splitting
        elif status == "ok":
            fileName = response.json()['link']
            filepath = os.path.join(app.root_path, f'static/uploads/youtube/{youtubeId}.mp3')
            
            # We don't want to save the video if it already exists
            if not os.path.isfile(filepath):
                urllib.request.urlretrieve(fileName, filepath)

    return response.json()


@youtubeApi.route('/youtube/mp3', methods=['POST'])
def youtubeMp3():
    vid = request.form.get('vid')
    app.logger.info(f"Starting convert to mp3 for youtube video ID: {vid}")
    
    try:
        # Get the response from the external REST Api
        response = convertToMp3(vid)
        
        if response['status'] != "ok":
            app.logger.error(f"Error in convert to mp3 for youtbe video ID: {vid} with message: {response['msg']}")
            return jsonify({'error': response['msg']})

        app.logger.info(f"Finished convert to mp3 with result: {response}")
        return jsonify({'success': response})

    # Catch any uncaught errors
    except Exception as e:
        app.logger.error(f"Error in convert to mp3 for youtube video ID: {vid} with message: {str(e)}")
        return jsonify({'error': str(e)})

    




