from flask import Flask
from logging.config import dictConfig
from socket import gethostname
import logging
import sys

DICT_CONFIG = {
    'version': 1,
    'formatters': {'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
        }, 'warning': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s at %(funcName)s: %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            'stream': 'ext://flask.logging.wsgi_errors_stream'
        },
        'file': {
            'class': 'logging.FileHandler',
            'level': 'WARNING',
            'formatter': 'warning',
            'filename': 'logs/error-logs.log'
        },
        'info': {
            'class': 'logging.FileHandler',
            'level': 'INFO',
            'formatter': 'default',
            'filename': 'logs/info-logs.log'
        }
    }, 'root': {
        'handlers': ('console', 'info', 'file'), 'level': 'INFO'
    }
}

if 'green-liveweb11' not in gethostname(): #if the application is deployed online
    dictConfig(DICT_CONFIG)
else: #if running locally 
    logging.basicConfig(stream=sys.stderr, level=logging.CRITICAL)

app = Flask(__name__)
app.config.from_object('config')

from .main.views import main
from .API.youtube import youtubeApi
from .API.separator import separatorApi
from .API.converter import converterApi

app.register_blueprint(main)
app.register_blueprint(youtubeApi)
app.register_blueprint(separatorApi)
app.register_blueprint(converterApi)