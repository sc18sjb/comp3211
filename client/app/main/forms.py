from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, FormField, IntegerField, SelectField
from wtforms.validators import DataRequired, Length, NumberRange

class YoutubeForm(FlaskForm):
    url = StringField('Url of video or video ID', validators=[
        DataRequired()
    ], render_kw={
        "placeholder": "Video ID"
    })

    submit = SubmitField('submit')


class SeparatorForm(FlaskForm):
    stems = SelectField('Output tracks', choices=[
        (2, 'Vocals + Accompaniment'), (4, 'Vocals + Drum + Base + Other'), (5, 'Vocals + Drum + Base + Other + Piano')
    ], validators=[
        DataRequired()
    ])

    bitrate = IntegerField('Bitrate (optional)', validators=[
        NumberRange(min=1, max=30000, message="Invalid bitrate")
    ], render_kw={
        "placeholder": "Bitrate"
    })

    filetype = SelectField('Filetype', choices=[
        ('mp3', 'Mp3'), ('wav', 'Wav'), ('ogg', 'Ogg'), ('flac', 'Flac')
    ], validators=[
        DataRequired()
    ])

    submit = SubmitField('submit')


'''
* Combine both forms into a tree of forms
* so that both can be rendered on the same page
'''
class AudioSplitterGroupForm(FlaskForm):
    youtube = FormField(YoutubeForm)
    separator = FormField(SeparatorForm)