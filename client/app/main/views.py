from app import app
from flask import render_template, url_for, Blueprint
from .forms import AudioSplitterGroupForm

main = Blueprint('main', __name__, template_folder='templates')

@main.route('/', methods=('GET', 'POST'))
@main.route('/index', methods=('GET', 'POST'))
def index():
    form = AudioSplitterGroupForm()

    return render_template('index.html',
                            form=form)