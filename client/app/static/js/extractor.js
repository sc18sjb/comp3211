export default class Extractor {
    constructor() {
        /**
         * We set this to self to we can continue using this
         * to refer to the context of elements
         */
        this.allowLoading = true;
        this.loading = false;
        this.loadingId = undefined;
        this.vid = undefined;
        this.stems = undefined;
        this.bitrate = undefined;
        this.filetype = undefined;
    }

    /**
     * Show the loading ring,
     * or if one doesn't exist create one
     */
    showSpinnerLoading() {        
        if (!this.allowLoading || this.loading)
            return;

        this.loading = true;

        /**
         * Check if the loader element already exists
         * and if not make it 
         */ 
        if ($('.loader.loader--spinner').length) {
            $('.loader.loader--spinner').fadeIn(50);
        } else {
            var html = `
                <div class="loader loader--spinner">
                    <img class="loader__img" src="/static/img/loading.svg" />
                    <p class="loading__text">Loading...</p>
                </div>
            `;
            $(html).hide().appendTo('body').fadeIn(50);
        }

        this.updateLoading(this);
    }

    /**
     * Hide the loading ring if the page is current loading
     */
    hideSpinnerLoading() {
        if (!this.loading)
            return;

        this.loading = false;

        $('.loader.loader--spinner').fadeOut(50);
        
        // Stop the updating loop
        if (this.loadingId !== undefined)
            clearTimeout(this.loadingId);
    }

    /**
     * Update the loading text to a random string 
     * just to make the interface look cooler
     */
    updateLoading(self) {
        if (!self.loading || !self.allowLoading)
            return

        // just some random statements for fun!
        var possibleStatements = [
            'Loading...', 'Untangling Spaghetti', 'Analyzing Hippos', 'Creating the 5th dimension', 'Sorting bits'
        ]
        const random = Math.floor(Math.random() * possibleStatements.length);
        $('.loading__text').text(possibleStatements[random]);
        self.loadingId = setTimeout(function() {
            self.updateLoading(self);
        }, 5000);
    }

    /**
     * Make a post request to one of the web services
     */
    post(url, data, callback) {
        $.ajax({
            method: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function(result) {
                callback(result);
            },
            error: function(error) {
                console.log(error);
                alert(error);
            }
        })
    }

    /**
     * submitVideoId takes the ID of a youtube video and 
     * calls an external youtube-to-mp3 API, saving the file
     * locally to the web server.
     * Returns information about the youtube video
     */
    extractMp3(vid, successCallback, errorCallback) {
        // If we're already loading then we don't want to process another request
        var self = this;

        // Error handling
        if (self.loading) {
            errorCallback({error: 'Still processing existing request, try again later'});
            return;
        } else if (vid === undefined) {
            errorCallback({error: 'Invalid video ID supplied'});
            return;
        }

        self.vid = vid;

        self.showSpinnerLoading();
        self.post('/youtube/mp3', [{name: 'vid', value: self.vid}], function(result) {
            self.hideSpinnerLoading();
            if (result.error) {
                self.vid = undefined;
                errorCallback(result);
            } else {
                result.success['vid'] = self.vid;
                successCallback(result);
            }
        });
    }   

    separateMp3(stems, successCallback, errorCallback) {
        var self = this;

        // Error handling
        if (self.loading) {
            errorCallback({error: 'Still processing existing request, try again later'});
            return;
        } else if (self.vid === undefined) {
            errorCallback({error: 'No video ID set'});
            return;
        } else if (stems === undefined) {
            errorCallback({error: 'Invalid output tracks supplied'});
            return;
        }

        self.stems = stems;
        
        self.showSpinnerLoading();
        self.post('/separate/mp3', [
            {name: 'vid', value: self.vid},
            {name: 'stems', value: self.stems}
        ], function(result) {
            self.hideSpinnerLoading()
            if (result.error) {
                errorCallback(result)
            } else 
                successCallback(result);
        });
    }

    convertWav(bitrate, filetype, successCallback, errorCallback) {
        var self = this;

        // Error handling
        if (self.loading) {
            errorCallback({error: 'Still processing existing request, try again later'});
            return;
        } else if (self.vid === undefined) {
            errorCallback({error: 'No video ID set'});
            return;
        } else if (bitrate === undefined) {
            errorCallback({error: 'Invalid bitrate supplied'});
            return
        } else if (filetype === undefined) {
            errorCallback({error: 'Invalid filetype supplied'});
            return;
        }

        self.bitrate = bitrate;
        self.filetype = filetype;

        self.showSpinnerLoading();
        self.post('/convert/wav', [
            {name: 'vid', value: self.vid},
            {name: 'bitrate', value: self.bitrate},
            {name: 'filetype', value: self.filetype}
        ], function(result) {
            self.hideSpinnerLoading();
            if (result.error) {
                self.bitrate = undefined;
                self.filetype = undefined;
                errorCallback(result);
            } else
                successCallback(result);
        });
    }
}