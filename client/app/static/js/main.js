import Extractor from './extractor.js';


class App {
    constructor(youtubeForm, separatorForm, bottomSheet) {
        this.youtubeForm = youtubeForm;
        this.separatorForm = separatorForm;
        this.bottomSheet = bottomSheet;
        this.extractor = new Extractor();
    }

     isValidUrl(url) {
        const matchpattern = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/gm;
        return matchpattern.test(url);
      }

    submitYoutubeForm() {
        var self = this;

        self.youtubeForm.submit(function(e) {
            e.preventDefault();

            if (self.separatorForm.css('display') !== 'none') 
                self.hideBottomSheet()

            var errorOuput = $(`#${$(this).attr('id')}__error`).fadeOut(50);
            var youtubeSubmit = $(this).find(':input[type="submit"]').prop('disabled', true);
            
            var youtubeLink = $(this).find('#youtube-url').val();
            if (self.isValidUrl(youtubeLink)) {
                if (youtubeLink.includes('v=')) {
                    youtubeLink = youtubeLink.split('?')[1].split('&')[0];
                    youtubeLink = youtubeLink.substring(2);  
                } else {
                    youtubeLink = youtubeLink.split('/')[3];
                }
            }

            self.extractor.extractMp3(youtubeLink, function(result) {
                console.log("Extract mp3 SUCCESS: ", result);
                youtubeSubmit.prop('disabled', false);

                self.setIframeValues(result.success);

                // wait for the iframe to load before proceeding
                $('#preview__iframe').ready(function() {
                    self.showBottomSheet();
                })
            }, function(error) {
                console.log("Extract mp3 ERROR: ", error);
                youtubeSubmit.prop('disabled', false);
        
                // Show the error
                errorOuput.text(error.error).fadeIn(50);
            });
        });
    }

    submitSeparatorForm() {
        var self = this;
        self.separatorForm.submit(function(e) {
            e.preventDefault();

            var disableInputs = function(disable = true) {
                self.youtubeForm.find(':input[type="submit"]').prop('disabled', disable);
                self.separatorForm.find(':input[type="submit"]').prop('disabled', disable);
            };
            
            var errorOutput = $(`#${$(this).attr('id')}__error`).fadeOut(50);
            disableInputs(true);

            self.showProgressLoading();

            self.extractor.separateMp3($(this).find('#separator-stems').val(), function(result) {
                console.log("Separate mp3 SUCCESS: ", result);
                self.showProgressLoading(50);
                self.convertWav(self, disableInputs, errorOutput);
            }, function(error) {
                console.log("Separate mp3 ERROR: ", error);
                disableInputs(false);
                self.hideProgressLoading();

                // Show the error
                errorOutput.text(error.error).fadeIn(50);
            });
        });
    }

    convertWav(self, disableInputs, errorOuput) {
        // Arguments for converter 
        let bitrate = self.separatorForm.find('#separator-bitrate').val();
        let filetype = self.separatorForm.find('#separator-filetype').val();

        self.extractor.convertWav(bitrate, filetype, function(result) {
            console.log("Convert from wav SUCCESS: ", result);
            self.showProgressLoading(100);

            // download the file
            document.location.href = '/' + result.success;
            disableInputs(false);

            // Hide the progress bar after an interval
            setTimeout(function() {
                self.hideProgressLoading();
            }, 1500)
        }, function(error) {
            console.log("Convert from wav ERROR: ", error);
            disableInputs(false);
            self.hideProgressLoading();

            // Show the error
            errorOuput.text(error.error).fadeIn(50);
        });
    }

    setIframeValues(data) {
        $('#preview__title--name').text(data.title);
        $('#preview__iframe').attr('src', `https://www.youtube.com/embed/${data.vid}?autoplay=0&showinfo=0&controls=0`);
    }

    showProgressLoading(amount = 10) {
        $(`#${this.bottomSheet.attr('id')}__loading`).fadeIn(50);
        $(`#${this.bottomSheet.attr('id')}__loading .progress-bar`).css('width', `${amount}%`);
    }

    hideProgressLoading() {
        $(`#${this.bottomSheet.attr('id')}__loading`).fadeOut(50);
        $(`#${this.bottomSheet.attr('id')}__loading .progress-bar`).css('width', '0%');
    }

    showBottomSheet() {
        this.separatorForm.fadeIn(50);
        this.bottomSheet.removeClass(`${this.bottomSheet.attr('id')}--closed`);
        this.extractor.allowLoading = false;
    }

    hideBottomSheet() {
        this.separatorForm.fadeOut(50);
        this.bottomSheet.addClass(`${this.bottomSheet.attr('id')}--closed`);
        this.extractor.allowLoading = true;
    }

    clickBottomSheet() {
        var self = this;
        $(`#${self.bottomSheet.attr('id')}__handle`).on('click', function(e) {
            e.preventDefault();
            self.hideBottomSheet();
        });
    }
}


$(document).ready(function() {
    let app = new App($('#video-id-form'), $('#separator-form'), $('#bottom-sheet'));
    app.submitYoutubeForm();
    app.submitSeparatorForm();
    app.clickBottomSheet();
});