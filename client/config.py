import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
dotenvPath = os.path.join(basedir, '.env')

load_dotenv(dotenvPath)

WTF_CSRF_ENABLED = True
SECRET_KEY = "599e2d63d8d676f5bf4eb35577bf7fc3"

X_RAPIDAPI_KEY = "529e92c144msh3a4d499284e1cb6p1cd38bjsn4c3db8a0dc3b"