# Web service 1 - Flask

A simple REST API for converting audio files to different types and compressing the bitrate.

## Installation and runing

`cp .env-example .env` (you should really change the values for the auth in this .env file)

`docker build -t converter .`

`docker run -it -p 80:8080 converter`
