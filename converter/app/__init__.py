from flask import Flask
from flask_restful import Api
from logging.config import dictConfig
from socket import gethostname
from flask_httpauth import HTTPBasicAuth
import logging
import sys

DICT_CONFIG = {
    'version': 1,
    'formatters': {'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
        }, 'warning': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s at %(funcName)s: %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            'stream': 'ext://flask.logging.wsgi_errors_stream'
        },
        'file': {
            'class': 'logging.FileHandler',
            'level': 'WARNING',
            'formatter': 'warning',
            'filename': 'logs/error-logs.log'
        },
        'info': {
            'class': 'logging.FileHandler',
            'level': 'INFO',
            'formatter': 'default',
            'filename': 'logs/info-logs.log'
        }
    }, 'root': {
        'handlers': ('console', 'info', 'file'), 'level': 'INFO'
    }
}

if 'green-liveweb11' not in gethostname(): #if the application is deployed online
    dictConfig(DICT_CONFIG)
else: #if running locally 
    logging.basicConfig(stream=sys.stderr, level=logging.CRITICAL)

app = Flask(__name__)
api = Api(app) 
app.config.from_object('config')
auth = HTTPBasicAuth()

from .resources.converter import ConverterBitrate, ConverterNoBitrate, ConverterNoFiletype

api.add_resource(ConverterNoFiletype, '/converter', endpoint='converter_no_filetype')
api.add_resource(ConverterNoBitrate, '/converter/<string:filetype>', endpoint='converter_no_bitrate')
api.add_resource(ConverterBitrate, '/converter/<string:filetype>/<int:bitrate>', endpoint='converter_bitrate')
