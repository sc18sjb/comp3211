from app import app, auth
from flask import send_file, request
from flask_restful import Resource, reqparse
from pydub import AudioSegment
import werkzeug
import os

AudioSegment.converter = os.path.join(app.root_path, 'static\\ffmpeg\\ffmpeg.exe')
AudioSegment.ffprobe = os.path.join(app.root_path, 'static\\ffmpeg\\ffprobe.exe')

@auth.verify_password
def verify(username, password):
    if not (username and password):
        return False
    return app.config['USER_DATA'].get(username) == password


def convertAudio(filetype="mp3", bitrate=None):
    # Parse the request arguments
    parse = reqparse.RequestParser()
    parse.add_argument('audio', type=werkzeug.datastructures.FileStorage, location='files', required=True)
    args = parse.parse_args()

    # Get the stream of bytes
    fileStream = args['audio'].stream
    fileExt = args['audio'].filename.split('.')[1]

    # Compress the file
    sound = AudioSegment.from_file(fileStream, format=str(fileExt))
    filePath = os.path.join(app.root_path, f'static/output/output.{filetype}')
    compressedSound = sound.export(filePath, format=filetype, bitrate=bitrate)

    return filePath


class ConverterBitrate(Resource):
    @auth.login_required
    def post(self, filetype, bitrate):
        # Return 
        app.logger.info(f"Begin audio conversion with bitrate: {bitrate} for file extension: {filetype}")
        return send_file(convertAudio(filetype, f"{bitrate}k"))


class ConverterNoBitrate(Resource):
    @auth.login_required
    def post(self, filetype):
        # Return 
        app.logger.info(f"Begin audio conversion with default bitrate and file extension: {filetype}")
        return send_file(convertAudio(filetype))


class ConverterNoFiletype(Resource):
    @auth.login_required
    def post(self):
        # Return 
        app.logger.info(f"Begin audio conversion with all default values")
        return send_file(convertAudio())


