import requests
from playsound import playsound
from requests.auth import HTTPBasicAuth

BASEURL = "http://176.58.123.162:80/"

fileExt = 'flac'
bitrate = 12

with open('boot.wav', 'rb') as audio:
    response = requests.post(BASEURL + f"converter/{fileExt}", files={'audio': audio}, auth=HTTPBasicAuth("admin", "test"))
    
    print(response)
    print(response.status_code)

    if response.status_code == 200:
        audio = response.content
        with open(f'convertedAudio.{fileExt}', 'wb') as f:
            f.write(audio)
            f.close()

        playsound(f'convertedAudio.{fileExt}')
        
    
