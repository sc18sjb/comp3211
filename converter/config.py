import os
from dotenv import load_dotenv

basedir = os.path.join(os.path.dirname(__file__))
dotenvPath = os.path.join(basedir, '.env')

load_dotenv(dotenvPath)

USER_DATA = {
    os.environ['AUTH_USER']: os.environ["AUTH_PWD"]
}