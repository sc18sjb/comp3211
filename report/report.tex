\documentclass[12pt,a4paper]{article}

\usepackage{amsmath}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{xcolor}
\usepackage{sectsty}
\usepackage{listings}
\usepackage{adjustbox}
\usepackage{siunitx}
\usepackage{hyperref}
\usepackage{longtable}
\usepackage{ltablex}
\usepackage{lipsum}
\usepackage{dirtree}

\newcommand{\verbatimfont}[1]{\renewcommand{\verbatim@font}{\ttfamily#1}}
\lstset{
basicstyle=\small\ttfamily,
columns=flexible,
breaklines=true
}

\begin{document}
\begin{titlepage}
	\centering
	{\scshape\LARGE University of Leeds \par}
	{\scshape\Large Facility of Engineering\par}
	{\scshape\Large School of Computing\par}
	\vspace{2cm}
	{\LARGE\bfseries COMP 3221\par}
	{\LARGE\bfseries Distributed Systems\par}
	{\LARGE\bfseries Coursework 2\par}
	\vspace{3cm}
	{\Large\itshape Sam Barnes/Calum wallbridge\par}
	{\Large\itshape sc18sjb/sc18csw \par}
	{\Large\itshape 201227622/{201249070} \par}
	\vfill
	
% Bottom of the page
	{\large \hrule
	\begin{flushleft}
		\textbf{Submission date:} 17th November 2021, 10:00\\
	\end{flushleft}
	\par}
\end{titlepage}

\newpage

\tableofcontents
\pagebreak

\section{Composition of Originality}

This report outlines a composition of services that allows users to separate music into separate sources.

We provide multiple flavours of separation, the canonical case being Vocals and Accompaniment, allowing users to specify the output type (mp3, ogg, ect.) as well as the quality.  Perhaps a useful tool for musicians who want a backing track to sing along to, or, for producers who want to isolate the drum beat to remix for another song.

The extremely easy web interface can simply require a YouTube URL as input, outputting the separate sources to be downloaded.

\pagebreak
\section{Web Service 1}

\begin{itemize}
	\item Name of student in charge: \textbf{Calum Wallbridge}
	\item Name of service: \textbf{Audio Splitter}
	\item SOAP-based or RESTful: \textbf{RESTful}
\end{itemize}

\subsection{Service Design}
The service is split into three packages: \emph{cmd}, \emph{separator} and \emph{server}.

The \emph{cmd} package holds our commands or entry points.

The \emph{separator} package is an isolated package just for source separation. 

The \emph{server} package holds the code for exposing the service on a network.
\\
Using this structure allows for easy extensiblity, readability and simultaneous development across teams.

Docker is then used to contain our running application which enables hosting and developement on any machine without complex setup.

\subsection{Service Implementation}
The implementation is done using Go.

For source separation pre-trained deep learning models are used with an auto encoder architecture trained with self supervision on millions of songs from deezer.  (see \url{github.com/deezer/spleeter})

The models are downloaded directly from github in the Dockerfile and invoked from our Go code within the deezer implementation of our separator.

For simplicity the server uses the local file system (default /var/separator) to store and cache files as they are processed.  gin is used as the http framework and we use multipart forms to send and receive the files.

\subsection{Service Invocation}
The service exposes a single endpoint \verb_POST /separate/:stems_ which expects a multipart form with the content of a single audio file in the form with the header 'file'.  The optional parameter \emph{stems} can be either 2, 4, or 5:
\begin{itemize}
	\item Vocals + Accompaniment (2 stems)
	\item Vocals + Drums + Bass + Other (4 stems)
	\item Vocals + Drums + Bass + Piano + Other (5 stems)
\end{itemize}


\subsection{Invocation through a client}
We have also exposed the service to the internet, so you can try yourself with any music file (examples are provided in \emph{/separator/test\_data}). The server will return all separated sources in a zip:
\begin{verbatim}
    curl -F file=@example.mp3 -JO 176.58.112.24/separate/2
\end{verbatim}

\subsection{Service Allocation time}

The server is instrumented to time requests, however, to calculate the following statistics we measure the request client side which will include network latency.

We can simply use \verb$curl$, as it has built in monitoring which is more than sufficient for this experiment.
\begin{lstlisting}
    seq 5 | xargs -I -- curl -w '%{time_total}\n' -s -o /dev/null -F file=@example.mp3 localhost/separate/2 > measure.txt
\end{lstlisting}
The -w option allows us to specify curls formatted output, we are only concerned about total time, but we could also output times here for namelook, connection, file transfer ect.  The -s and -o /dev/null ensures all other output is silenced.
We redirect this output to a file where we can perform some simple analysis, such and mean and standard deviation:
\begin{verbatim}
   awk '{x+=$0;y+=$0^2}END{print sqrt(y/NR-(x/NR)^2)}' measure.txt
   awk '{x+=$0}END{print x/NR}' measure.txt
\end{verbatim}

\begingroup
\setlength{\LTleft}{-20cm plus -1fill}
\setlength{\LTright}{\LTleft}
\begin{longtable}{ |p{5.5cm}|p{6cm}| }
	
	\hline
	\textbf{Run No.} & \textbf{Service Invocation Time (s)} \\
	\hline
	
	1 & 13.871603 \\
	\hline
	
	2 & 14.140608 \\
	\hline
	
	3 & 14.377957 \\
	\hline	
	
	4 & 14.234195 \\
	\hline
	
	5 & 14.055330 \\
	\hline
	
	Mean & 14.14 \\
	\hline
	
	Standard Deviation & 00.17 \\
	\hline

\end{longtable}
\endgroup

\pagebreak
\section{Web Service 2}

\begin{itemize}
	\item Name of student in charge: \textbf{Samuel Barnes}
	\item Name of service: \textbf{Audio Converter}
	\item SOAP-based or RESTful: \textbf{RESTful} 
\end{itemize}

\noindent
The second web service, \textit{Audio Converter}, takes an audio file as its input in any format (this can be mp3, wav, ogg, flac etc.) and converts it to a different specified audio file format. This new converted file is then returned to the user. The user can also specify the bitrate of the audio file to be returned and, in this case, the service will compress the audio file to the specified bitrate and return the new audio file.

This service has been deployed and is available online!
\begin{itemize}
	\item URL: \textbf{http://176.58.123.162/}
	\item git: \textbf{https://gitlab.com/sc18sjb/comp3211/-/tree/main/converter}
	\item Invocation: \textbf{described below}
\end{itemize}

\subsection{Service Design}
The web service has been designed as an abstraction of the resources and API endpoints. That is, each endpoint is represented as its own resource in the hierarchy and are then 'hooked' up to the API interface, exposing them through different routes. Each resource has access to a collection of utility functions which may be needed to process the request. In designing the service this way allows it to be easily extendible; adding a new route is as simple as creating a new resource implemented with the utility functions and hooking this resource to an API endpoint.

The \textit{Audio Converter} service needs to save posted files to the server locally in order to process them for conversion and compression. The service is therefore designed to have an uploads location where these files can be stored in a static folder. The service will save each uploaded file only temporarily as a cache, since when the conversion has taken place, these files will no longer be needed and just take up unnecessary space on the server.

Below shows the design for a potential file structure the service can use:\\

\dirtree{%
.1 API Interface.
.1 resources/.
.2 resource 1.
.2 resource 2.
.2 ....
.2 utils/.
.3 utility functions.
.1 static/.
.2 output/.
.3 output 1.
.3 output 2.
.3 ....
}


\subsection{Service Implementation}
The web service has been implemented using Flask and the Flask-RESTful packages for Python. Each endpoint is represented as a \textit{Resource} object in the Flask-RESTful package and is then added to our Flask API variable to expose them through the interface. When we add the resource to the API, we define the endpoint to create the different routes of the API.

Each Resource is implemented to parse the request sent to the API using the Flask-RESTful \textit{RequestParser()} object. This extracts the file sent as a stream of bytes to the Resource allowing the service to carry out the conversion and compression of the file. That itself is implemented using the pydub package, allowing us to specify the bitrate to compress to, the filetype to convert to, as well as other options such as setting cover art for the audio file. As mentioned in the above section, \textit{Service Design}, the audio file is saved locally since this is needed to send the file back to the client using the Flask \textit{send\_file()} function (which sends the file as a stream of bytes). 

The service implements basic authentication using the Flask-httpauth \textit{HTTPBasicAuth} object. There is a helper function that checks if the auth credentials sent to the service are correct, and this function is included as a decorator for each POST and GET request in each defined resource. 

The service is ran in a Python virtual enviroment so that only the packages needed are installed. The service has been hosted at the IP address: \textbf{http://176.58.123.162/} on Linode simply by running the server in the virtual environment as a background process. The service implement Flask's logging functionality to log info and errors to the server to help debug any issues that might arise. You can learn how to use the service in the below section \textit{Service Invocation}.

\subsection{Service Invocation}
The web service has 3 routes which can be used for invocation. All of these are POST requests allowing you to push your audio file to the service, specifying the return filetype and bitrate in the query parameters of the service call.

\begin{itemize}

	\item \textbf{Required Parameters:} The following table specifies the required parameters when making a call to the \textit{Audio Converter} API:

\begingroup
\setlength{\LTleft}{-20cm plus -1fill}
\setlength{\LTright}{\LTleft}
\begin{longtable}{ |p{5.5cm}|p{6cm}| }
	\hline
	\textbf{Required parameter} & \textbf{Description}\\
	\hline
	
	audio \{FILE\} & The file which will be converted by the converter API \\
	\hline
	
	auth \{Basic Auth\} & Basic auth is required to get a response. The current credentials are (usrname: \textbf{'admin'}, password: \textbf{'test'})\\
	\hline
	
\end{longtable}
\endgroup

	\item \textbf{Endpoints:} The below table species the endpoints available from the \textit{Audio Converter} API, what they do and an example call with the Python requests module:

\begingroup
\setlength{\LTleft}{-20cm plus -1fill}
\setlength{\LTright}{\LTleft}
\begin{longtable}{ |p{5.5cm}|p{4cm}|p{7.5cm}| }
		\hline 
		\textbf{Endpoint} & \textbf{Description} & \textbf{Example - Python} \\
		\hline 
		
		/converter & convert a file to the default filetype which is mp3, with the default bitrate (that of the current file). & 
		\fontsize{10pt}{12pt}\selectfont
		\begin{verbatim}
		import requests
		from requests.auth import HTTPBasicAuth
		
		BASEURL = 'http://176.58.123.162/'
		audio = open(AUDIOFILEPATH, 'rb')
		
		response = requests.post(
		    f'{BASEURL}converter',
		    files={'audio':audio}
		    auth=HTTPBasicAuth('admin', 'test')	
		)
		
		newaudio = response.content
		\end{verbatim} \\
		\hline 
		
		/converter/\{fileExt\} & Convert a file to a specified filetype (mp3, wav, flax, ogg), with the default bitrate (that of the current file). & 
		\fontsize{10pt}{12pt}\selectfont
		\begin{verbatim}
		import requests
		from requests.auth import HTTPBasicAuth
		
		BASEURL = 'http://176.58.123.162/'
		audio = open(AUDIOFILEPATH, 'rb)
		fileExt = 'wav'
		
		response = requests.post(
		    f'{BASEURL}/converter/{fileExt}',
		    files={'audio':audio},
		    auth=HTTPBasicAuth('admin', 'test')	
		)		
		
		newaudio = response.content
		\end{verbatim} \\
		\hline 
		/converter/\{fileExt\}/\{bitrate\} & Convert a file to a specified filetype (mp3, wav, flax, ogg), and with a specified integer bitrate. &
		\fontsize{10pt}{12pt}\selectfont
		\begin{verbatim}
		import requests
		from requests.auth import HTTPBasicAuth
		
		BASEURL = 'http://176.58.123.162/'
		audio = open(AUDIOFILEPATH, 'rb)
		fileExt = 'wav'
		br = 126 # bitrate
		
		response = requests.post(
			f'{BASEURL}/converter/{fileExt}/{br}',
			files={'audio':audio},
			auth=HTTPBasicAuth('admin', 'test')	
		)		
		
		newaudio = response.content
		\end{verbatim} \\
		\hline 
		
\end{longtable}
\endgroup

	\item \textbf{Response:} The \textit{Audio Converter} API will return a converted audio file as a stream of bytes. This can then be saved to the clients machine by being written to a new file. Below is an example of dealing with a response in python: 

\begin{verbatim}
if response.status_code == 200:
    newaudio = response.content

    with open('convertedAudio.FILEEXT', 'wb' as f:
        f.write(newaudio)
        f.close()
   
\end{verbatim}

Replace '.FILEEXT' with the file extension of the converted audio.

\end{itemize}

We will walk through a full example of the \textit{Audio Converter} API invocation with a Python example in the next section \textit{Invocation Through a Client}.

\subsection{Invocation Through a Client}
The \textit{Audio Converter} web service was tested using a Python client. In the example below we are calling the \textit{Audio Converter} web service's second route '/converter/\{fileExt\}, specifying the file extension as 'flac'. This means the service will return a converted audio in 'flac' format with the default bitrate. The file we are sending, 'boot.wav', is just an audio file found locally on my computer.

\begin{verbatim}
import requests
from playsound import playsound
from requests.auth import HTTPBasicAuth

BASEURL = "http://176.58.123.162:80/"
fileExt = 'flac'

with open('boot.wav', 'rb') as audio:
    response = requests.post(
        BASEURL + f"converter/{fileExt}",
        files={'audio': audio}, auth=HTTPBasicAuth("admin", "test")
    )
    
    if response.status_code == 200:
        audio = response.content
        with open(f'convertedAudio.{fileExt}', 'wb') as f:
            f.write(audio)
            f.close()
            
        playsound(f'convertedAudio.{fileExt}')
\end{verbatim}

The response simply plays the converted audio on the clients machine, signifying the process has worked correctly!

\begin{center}
	\includegraphics[width=1\textwidth]{converter_evidence.png}
	
	\textbf{Response from service invocation}
\end{center}

I modified my client to print out the response and status code (shown above) as evidence of the services execution through the client (since I cannot show the audio file playing). We can see the response status 200 which means the request completed successfully. Checking the folder where the client is called, a new file 'convertedAudio.flac' has been created (shown below)!

\begin{center}
	\includegraphics[width=1\textwidth]{converter_evidence_2.png}
	
	\textbf{'convertedAudio.flac' in folder}
\end{center}



\subsection{Service Allocation Time}

\begingroup
\setlength{\LTleft}{-20cm plus -1fill}
\setlength{\LTright}{\LTleft}
\begin{longtable}{ |p{5.5cm}|p{6cm}| }
	
	\hline
	\textbf{Run No.} & \textbf{Service Invocation Time} \\
	\hline
	
	1 & 0.430117 seconds \\
	\hline
	
	2 & 0.570314 seconds \\
	\hline
	
	3 & 0.505176 seconds \\
	\hline	
	
	4 & 0.402647 seconds \\
	\hline
	
	5 & 0.826208 seconds \\
	\hline
	
	Average & 0.5468924 seconds\\
	\hline
	
	Standard Deviation & 0.106385743 seconds\\
	\hline

\end{longtable}
\endgroup

The service allocation time of the web service \textit{Audio Converter} is highly dependant on the file passed to it: large files will take longer to process and thus take longer to invoke, smaller files will take less time. Because of this, when testing the web service through the client described in the section \textit{Invocation Through a Client}, I am keeping the file passed to the API as a constant. In this case I am using the file 'boot.wav' which is 2.44MB in size.

I am testing the web service through the route 'converter/\{fileExt\}', converting to the 'flac' filetype. I am running the test $n$ times where $n = 5$. We can see from the results there all the service allocation times took less than 1 second, averaging around half a second. There is a standard deviation of 0.1 seconds between the results, this is likely caused by other processes on my computer which could be slowing down the request, i.e. other requests taking place at the same time or higher ping on my network. The web service itself could be handling multiple concurrent requests. There is too many variables to say for sure what could be causing the difference, but with a deviation of only 0.1 seconds there is no need to look further into the matter.

\pagebreak
\section{Web Service 3 - External Service}

\begin{itemize}
	\item Name of service: \textbf{YouTube MP3}
	\item SOAP-based or RESTful: \textbf{RESTful}
	\item Name of publisher: \textbf{RapidApi}
	\item URL: \textbf{https://youtube-mp36.p.rapidapi.com/dl}
\end{itemize}

The third web service, \textit{YouTube MP3} is an external RESTful web service that is available as one of the many API's on the RapidAPI repository. The web service does exactly what you would think; takes a YouTube video ID and converts this to an MP3 file that is returned to the client.

\subsection{Service Invocation}
There is a very well documented description of how the web service is invoked, the required parameters and allowed endpoints on the API's documentation available at: \textbf{https://rapidapi.com/ytjar/api/youtube-mp36/}. 

To summarise, you invoke the web service by making a GET request to the URL: https://youtube-mp36.p.rapidapi.com/dl, passing the YouTube video ID (not URL!) as a required parameter. In order for the request to be accepted you must have a RapidAPI key which is set in the headers of the GET request.

The code below shows how the service is invoked on the client side using the Python requests module:

\begin{verbatim}
import requests
import urllib.request
from threading import Timer

def convert(id):
    headers = {
        'x-rapidapi-host': "youtube-mp36.p.rapidapi.com",
        ''x-rapidapi-key': APIKEYHERE
    }

    response = requests.get(
        "https://youtube-mp36.p.rapidapi.com/dl",
        headers=headers,
        params={'id': id}
    )
    
    if response.status_code == 200:
        status = response.json()['status']
        
        if status == 'processing':
            rerun = Timer(2.0, convertToMp3, args=[id])
            rerun.start()
        else:
            fileName = response.json()["link"]
            urllib.request.urlretrieve(fileName, 'youtube.mp3')    
\end{verbatim}

We call the API which as a response may return the status 'processing'. In this instance we need to re-call the API by making another request after a delay (in this case 2 seconds) and check the status again until 'ok'. The API returns a URL for the final MP3 file which can then be retrieved and downloaded onto the clients machine.

\subsection{Invocation Through a Client}
We can invoke the \textit{YouTube MP3} web service through a client using the code in the above section: \textit{Service Invocation}, passing through a YouTube video ID. The MP3 file will then be saved locally to the clients machine.

\begin{verbatim}
convert('dQw4w9WgXcQ')
\end{verbatim}

In this example we are requesting the video ID 'dQw4w9WgXcQ' and saving it to the file 'youtube.mp3'. We can modify the \textit{convert(id)} function to print out the response and status code from the API call:

\begin{center}
	\includegraphics[width=1\textwidth]{youtube_evidence.png}
	
	\textbf{Response from service invocation}
\end{center}

We can see the response status 200 which means the request completed successfully. The API returns a JSON object containing a link to the file, the title of the video, the duration of the service allocation time, the status and corresponding progress as well as the progress (of how long the video has left to convert if status = 'processing'). We can see this evidences above.

Checking the folder where the client is called we see a new file 'youtube.mp3' created, showing the client has called the \textit{YouTube MP3} service successfully and saved the file from the link provided in the response.

\begin{center}
	\includegraphics[width=1\textwidth]{youtube_evidence_2.png}
	
	\textbf{'youtube.mp3' in folder}
\end{center}


\subsection{Service Allocation Time}

\begingroup
\setlength{\LTleft}{-20cm plus -1fill}
\setlength{\LTright}{\LTleft}
\begin{longtable}{ |p{5.5cm}|p{6cm}| }
	
	\hline
	\textbf{Run No.} & \textbf{Service Invocation Time} \\
	\hline
	
	1 & 6.478128 seconds \\
	\hline
	
	2 & 0.683544 seconds \\
	\hline
	
	3 & 5.839841 seconds \\
	\hline	
	
	4 & 0.552774 seconds \\
	\hline
	
	5 & 5.881057 seconds \\
	\hline
	
	Average & 3.8870688 seconds\\
	\hline
	
	Standard Deviation & 2.678917279 seconds\\
	\hline

\end{longtable}
\endgroup

The service allocation time for the web service \textit{YouTube MP3}is highly dependant on the YouTube Video ID passed to it:  if its a long video then the service will take longer to process and convert to MP3, if its a short video this will be done quicker. To keep the test fair, we use a constant video ID between each of the tests, in this case I will use the same video ID as in the above section \textit{Invocation Through a Client}: \textbf{dQw4w9WgXcQ} which is a 3 minute and 32 second long video. 

I am running the test $n$ times where $n=5$. We can see from the results that there is a pattern between each group of 2 calls (1-2, 3-4, 5-...): the first call is longer, around 6 seconds and the second call is shorter, around 0.5 seconds. The reasonable explanation for this is that the \textit{YouTube MP3} service is caching the downloaded mp3 so that when we make a proceeding call it just returns the already converted MP3 file. It only seems to cache for 1 proceeding call however, so when we make a 3rd call for example we have to convert and download the MP3 again. This leads to a relatively large standard deviation of around 2.5 seconds.


\pagebreak
\section{Web service Integration}
Web Service 1 takes in a file and returns a file.  Web Service 2 takes in a file and returns a file.  The external web service simple takes in a URL and returns a file.

These simple interfaces allow us to effortlessly integrate them.  For our purporses we start with the external web service.  The user inputs a YouTube URL (we do some validation and extract the video ID) and we request the video content.  From here we can so what we can with the video.  Passing it between services as we require.

We use a basic flask server to server raw html and scss files to the user.  The server defines 2 forms, one for the Youtube URL and the other for separation details, including the number of output stems the users requires, output file types and quality.  For simplicity this server also exposes an endpoint for each service that can be called when the appropriate forms are filled in.

\section{Successful Execution}


\section{Code Submission}


\section{Video Submission}


\section{Online Deployment}


\end{document}

