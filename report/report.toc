\contentsline {section}{\numberline {1}Composition of Originality}{2}{section.1}% 
\contentsline {section}{\numberline {2}Web Service 1}{3}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Service Design}{3}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Service Implementation}{3}{subsection.2.2}% 
\contentsline {subsection}{\numberline {2.3}Service Invocation}{3}{subsection.2.3}% 
\contentsline {subsection}{\numberline {2.4}Invocation through a client}{4}{subsection.2.4}% 
\contentsline {subsection}{\numberline {2.5}Service Allocation time}{4}{subsection.2.5}% 
\contentsline {section}{\numberline {3}Web Service 2}{5}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Service Design}{5}{subsection.3.1}% 
\contentsline {subsection}{\numberline {3.2}Service Implementation}{6}{subsection.3.2}% 
\contentsline {subsection}{\numberline {3.3}Service Invocation}{7}{subsection.3.3}% 
\contentsline {subsection}{\numberline {3.4}Invocation Through a Client}{9}{subsection.3.4}% 
\contentsline {subsection}{\numberline {3.5}Service Allocation Time}{10}{subsection.3.5}% 
\contentsline {section}{\numberline {4}Web Service 3 - External Service}{12}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Service Invocation}{12}{subsection.4.1}% 
\contentsline {subsection}{\numberline {4.2}Invocation Through a Client}{13}{subsection.4.2}% 
\contentsline {subsection}{\numberline {4.3}Service Allocation Time}{14}{subsection.4.3}% 
\contentsline {section}{\numberline {5}Web service Integration}{15}{section.5}% 
\contentsline {section}{\numberline {6}Successful Execution}{15}{section.6}% 
\contentsline {section}{\numberline {7}Code Submission}{15}{section.7}% 
\contentsline {section}{\numberline {8}Video Submission}{15}{section.8}% 
\contentsline {section}{\numberline {9}Online Deployment}{15}{section.9}% 
