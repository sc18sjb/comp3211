# Web service 2 - Separator

Separator is a service that can separate music out into separte streams.

i.e. given an audio file with stems=2 it will return 2 audio files, one with only the instrumentals and the other with just the vocals.

Stems can be 2, 4 or 5.

For best performance, a GPU is needed, but for demonstration we ignore this.

## Requirements

- Docker

## Start the server

`docker build -t separator .`

`docker run -it -p 80:8080 separator`

## Examples

`curl -F file=@example.mp3 -OJ 'localhost/separate/2`


