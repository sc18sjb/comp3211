package main

import (
	"flag"
	"os"

	"separator/separator"
	"separator/server"

	"github.com/gin-gonic/gin"
)

var (
	addr = flag.String("addr", ":8080", "The address to listen on.")
	wd   = flag.String("wd", "/var/separator", "The internal server working directory.")
)

func main() {
	flag.Parse()
	os.Mkdir(*wd, 6)

	router := &server.Router{
		Engine:           gin.Default(),
		WorkingDirectory: *wd,
		Separator:        separator.New(),
	}

	router.Use(router.CORSMiddleware)
	router.POST("/separate/*stems", router.SeparateStems)
	router.Run(*addr)
}
