package separator

import (
	"fmt"
	"os/exec"
)

type deezerSeparator struct{}

func (s *deezerSeparator) Separate(req Request) error {
	cmd := exec.Command("spleeter", "separate",
		"-o", req.OutputDir,
		"-p", fmt.Sprintf("spleeter:%dstems", req.Stems),
		req.FilePath)

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("ERROR|deezerSeparator|Separate|req: %+v|%v", req, err)
	}

	return nil
}
