package separator

type Separator interface {
	Separate(Request) error
}

type Request struct {
	// FilePath is the file to be separated
	FilePath string
	// OutputDir is where the result should be saved
	OutputDir string
	// Stems is the desired number of output tracks
	Stems int
}

// GetSeparator for now gives us a new deezerSeparator
func New() Separator {
	return &deezerSeparator{}
}
