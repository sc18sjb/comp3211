package server

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"separator/separator"

	"github.com/gin-gonic/gin"
)

// Router is our gin-based http router
type Router struct {
	*gin.Engine
	WorkingDirectory string
	Separator        separator.Separator
}

func (r *Router) CORSMiddleware(c *gin.Context) {
	// Expose the relavent headers
	c.Header("Access-Control-Expose-Headers", "Content-Disposition Content-Type")
	c.Next()
}

func (r *Router) Separate(c *gin.Context) {
	r.SeparateStems(c)
}

// SeparateStems
func (r *Router) SeparateStems(c *gin.Context) {
	fileIn, err := c.FormFile("file")
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("get file err: %s", err.Error()))
		return
	}

	// each upload gets it's own directory
	dir, err := ioutil.TempDir(r.WorkingDirectory, "upload_")
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, fmt.Errorf("get form err: %s", err.Error()))
		return
	}
	dst := filepath.Join(dir, fileIn.Filename)
	if err := c.SaveUploadedFile(fileIn, dst); err != nil {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("upload file err: %s", err.Error()))
		return
	}

	// separate
	err = r.Separator.Separate(separator.Request{FilePath: dst, OutputDir: dir, Stems: getStems(c.Param("stems"))})
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("separate err: %s", err.Error()))
		return
	}

	// remove the original file to prevent zipping it up
	os.Remove(dst)

	file, err := ioutil.TempFile("", "output_*.zip")
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("create tmp err: %s", err.Error()))
		return
	}
	defer file.Close()

	err = zipFiles(file, dir)
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("zip file err: %s", err.Error()))
		return
	}

	c.Header("Content-Type", "application/zip")
	c.Header("Content-Disposition", "attachment; filename='split_audio.zip'")

	// Must set the offset back to zero before writing it to the ResponseWriter
	_, err = file.Seek(0, io.SeekStart)
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("seek zip file err: %s", err.Error()))
		return
	}

	_, err = io.Copy(c.Writer, file)
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("copy zip file err: %s", err.Error()))
		return
	}

	c.Status(200)
}

// getStems returns the integer value of stems when possible
// or default value of 2
func getStems(s string) (stems int) {
	fmt.Println(s)
	switch s {
	case "2":
		stems = 2
	case "4":
		stems = 4
	case "5":
		stems = 5
	default:
		stems = 2
	}
	return
}
