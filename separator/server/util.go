package server

import (
	"archive/zip"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

// zipFiles traverses the given directory (including sub-directories)
// and writes each file to the root of the given zipFile
func zipFiles(zipFile *os.File, directory string) error {
	zipw := zip.NewWriter(zipFile)
	defer zipw.Close()

	walker := func(path string, info os.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		file, err := os.Open(path)
		if err != nil {
			return fmt.Errorf("can't open file %s|%v", path, err)
		}
		defer file.Close()

		// taking the base of the path puts all files in root
		zipFile, err := zipw.Create(filepath.Base(path))
		if err != nil {
			return fmt.Errorf("can't create zip file %s|%v", path, err)
		}

		_, err = io.Copy(zipFile, file)
		if err != nil {
			return fmt.Errorf("can't copy file %s  into zip|%v", path, err)
		}

		return nil
	}

	return filepath.WalkDir(directory, walker)
}
